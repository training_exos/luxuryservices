<?php

namespace App\Repository;

use App\Entity\Joboffers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Joboffers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Joboffers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Joboffers[]    findAll()
 * @method Joboffers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoboffersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Joboffers::class);
    }

    // /**
    //  * @return Joboffers[] Returns an array of Joboffers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Joboffers
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
