<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190718121333 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Candidats ADD updated_at DATETIME NOT NULL, ADD passport_name VARCHAR(255) NOT NULL, ADD passport_size INT NOT NULL, ADD cv_name VARCHAR(255) NOT NULL, ADD cv_size INT NOT NULL, ADD profilepicture_name VARCHAR(255) NOT NULL, ADD profilepicture_size INT NOT NULL, DROP PassportUrl, DROP CVUrl, DROP ProfilePictureUrl, CHANGE Gender Gender VARCHAR(255) DEFAULT NULL, CHANGE FirstName FirstName VARCHAR(100) NOT NULL, CHANGE LastName LastName VARCHAR(100) NOT NULL, CHANGE Adress Adress VARCHAR(100) DEFAULT NULL, CHANGE Country Country VARCHAR(100) DEFAULT NULL, CHANGE Nationality Nationality VARCHAR(100) DEFAULT NULL, CHANGE HasPassport HasPassport TINYINT(1) DEFAULT NULL, CHANGE CurrentLocation CurrentLocation VARCHAR(100) DEFAULT NULL, CHANGE BirthDate BirthDate DATE DEFAULT NULL, CHANGE BirthPlace BirthPlace VARCHAR(100) DEFAULT NULL, CHANGE roles roles JSON NOT NULL, CHANGE Availability Availability TINYINT(1) DEFAULT NULL, CHANGE Experience Experience VARCHAR(255) DEFAULT NULL, CHANGE CreatedAT CreatedAT DATE DEFAULT NULL, CHANGE DeletedAt DeletedAt DATE DEFAULT NULL, CHANGE CategoryId CategoryId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Clients CHANGE EntrepriseType EntrepriseType VARCHAR(100) DEFAULT \'NULL\', CHANGE ContactPosition ContactPosition VARCHAR(100) DEFAULT \'NULL\', CHANGE ContactTel ContactTel VARCHAR(100) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE JobOffers CHANGE JobTitle JobTitle VARCHAR(100) DEFAULT NULL, CHANGE JobType JobType VARCHAR(255) DEFAULT NULL, CHANGE ClosingDate ClosingDate DATE DEFAULT NULL, CHANGE Salary Salary INT DEFAULT NULL, CHANGE CreationDate CreationDate DATE DEFAULT NULL, CHANGE ClientId ClientId INT DEFAULT NULL, CHANGE CategoryId CategoryId INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Candidats ADD PassportUrl VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, ADD CVUrl VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, ADD ProfilePictureUrl VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, DROP updated_at, DROP passport_name, DROP passport_size, DROP cv_name, DROP cv_size, DROP profilepicture_name, DROP profilepicture_size, CHANGE Gender Gender VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE FirstName FirstName VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE LastName LastName VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE Adress Adress VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE Country Country VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE Nationality Nationality VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE HasPassport HasPassport TINYINT(1) DEFAULT \'NULL\', CHANGE CurrentLocation CurrentLocation VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE BirthDate BirthDate DATE DEFAULT \'NULL\', CHANGE BirthPlace BirthPlace VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin, CHANGE Availability Availability TINYINT(1) DEFAULT \'NULL\', CHANGE Experience Experience VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE CreatedAT CreatedAT DATE DEFAULT \'NULL\', CHANGE DeletedAt DeletedAt DATE DEFAULT \'NULL\', CHANGE CategoryId CategoryId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Clients CHANGE EntrepriseType EntrepriseType VARCHAR(100) DEFAULT \'\'NULL\'\' COLLATE utf8mb4_unicode_ci, CHANGE ContactPosition ContactPosition VARCHAR(100) DEFAULT \'\'NULL\'\' COLLATE utf8mb4_unicode_ci, CHANGE ContactTel ContactTel VARCHAR(100) DEFAULT \'\'NULL\'\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE JobOffers CHANGE JobTitle JobTitle VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE JobType JobType VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE ClosingDate ClosingDate DATE DEFAULT \'NULL\', CHANGE Salary Salary INT DEFAULT NULL, CHANGE CreationDate CreationDate DATE DEFAULT \'NULL\', CHANGE ClientId ClientId INT DEFAULT NULL, CHANGE CategoryId CategoryId INT DEFAULT NULL');
    }
}
