<?php

namespace App\Form;

use App\Entity\Candidats;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Female' => 'Female',
                    'Male' => 'Male',
                    'None' => 'None']
            ])
            ->add('firstname')
            ->add('lastname')
            ->add('adress')
            ->add('country')
            ->add('nationality')
            // ->add('passporturl')
            ->add('passportFile', VichFileType::class, [
                'required' => false,
            ])
            // ->add('cvurl')
            ->add('cvFile', VichFileType::class, [
                'required' => false,
            ])

            ->add('profilepictureFile', VichImageType::class, [
                'required' => false,
                'image_uri' => false,
            ])
            ->add('currentlocation')
            ->add('birthdate')
            ->add('birthplace')
            ->add('email')
            // ->add('roles')
            ->add('password', PasswordType::class)
            ->add('availability')
            ->add('experience')
            ->add('description')
            ->add('categoryid')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Candidats::class,
        ]);
    }
}
